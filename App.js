import React from 'react';
import {StyleSheet} from 'react-native';
import {Scene, Router, Stack, Tabs} from 'react-native-router-flux';
import Dashboard from './src/screens/Dashboard';
import Calendar from './src/screens/Calendar';
import Medication from './src/screens/Medication';
import MedicationDetails from './src/screens/MedicationDetails';
import TabBar from './src/components/TabBar';

const App = () => (
  <Router sceneStyle={styles.root}>
    <Stack key="root" hideNavBar={true}>
      <Tabs key="tabBar" tabBarComponent={TabBar}>
        <Scene key="tab1" component={Dashboard} title="Dashboard" />
        <Stack key="tab2">
          <Scene key="tab2_1" component={Medication} title="Medication" />
          <Scene
            key="medsDetails"
            component={MedicationDetails}
            title="Medication Details"
          />
        </Stack>
        <Scene key="tab3" component={Calendar} title="Calendar" />
      </Tabs>
    </Stack>
  </Router>
);

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#F9FDFD',
  },
});

export default App;
