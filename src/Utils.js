export const modArray = (array, value) => {
  const index = array.indexOf(value);

  if (!~index) {
    array.push(value);
  } else {
    array.splice(index, 1);
  }
  return [...array];
};
