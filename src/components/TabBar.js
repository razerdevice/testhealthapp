import React from 'react';
import {Image, StyleSheet, TouchableOpacity, View, SafeAreaView} from 'react-native';
import {Grayscale} from 'react-native-color-matrix-image-filters';
import {Actions} from 'react-native-router-flux';
import Assets from '../Assets';

const TabBar = props => {
  const {state} = props.navigation;
  const activeTabIndex = state.index;
  return (
    <SafeAreaView>
      <View style={styles.tabBar}>
        {state.routes.map((element, index) => (
          <TouchableOpacity
            key={element.key}
            onPress={() => Actions[element.key]()}>
            {activeTabIndex === index ? <Image source={Assets[element.key]} /> : (
              <Grayscale>
                <Image source={Assets[element.key]} />
              </Grayscale>
            )}
          </TouchableOpacity>
        ))}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  tabBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
    borderTopWidth: 1,
    borderColor: '#E0E0E0',
  },
});

export default TabBar;
