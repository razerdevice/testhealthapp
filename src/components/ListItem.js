import {CheckBox, Text} from 'native-base';
import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import moment from 'moment';

const ListItem = ({id, name, time, onPress, check, isChecked}) => {
  return (
    <TouchableOpacity
      onPress={() => onPress && onPress()}
      style={styles.listItem}>
      <Text>{moment(time).format('HH:mm')}</Text>
      <Text>{name}</Text>
      <CheckBox onPress={() => check(id)} checked={isChecked} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  listItem: {
    flexDirection: 'row',
    width: '100%',
    height: 40,
    marginVertical: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingEnd: 15,
  },
});

export default ListItem;
