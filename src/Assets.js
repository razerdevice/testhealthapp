import Home from '../assets/home.png';
import Calendar from '../assets/calendar.png';
import Pill from '../assets/pill.png';

export default {
  tab1: Home,
  tab2: Pill,
  tab3: Calendar,
};
