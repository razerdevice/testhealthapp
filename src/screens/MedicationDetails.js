import {Card, CardItem} from 'native-base';
import {View, StyleSheet, Text} from 'react-native';
import React from 'react';
import moment from 'moment';

const MedicationDetails = props => {
  const {
    item: {name, time},
  } = props;
  return (
    <View style={styles.container}>
      <Card>
        <CardItem header bordered style={styles.cardHeader}>
          <View style={{paddingVertical: 30}}>
            <Text style={[styles.text, {fontSize: 24}]}>{name}</Text>
            <Text style={styles.text}>{moment(time).format('HH:mm')}</Text>
          </View>
        </CardItem>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  cardHeader: {
    justifyContent: 'center',
  },
  text: {
    marginVertical: 10,
    textAlign: 'center',
    fontSize: 20,
  },
});

export default MedicationDetails;
