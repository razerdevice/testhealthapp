import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {Card, CardItem, Text, Body} from 'native-base';
import Person from '../../assets/user.png';

const Dashboard = () => (
  <View style={styles.container}>
    <View style={styles.greet}>
      <Image source={Person} style={styles.image} />
      <Text style={styles.greetText}>Good morning, Eugen</Text>
    </View>
    <Card>
      <CardItem header bordered style={styles.cardHeader}>
        <Text>Messages</Text>
      </CardItem>
      <CardItem bordered>
        <Body style={styles.cardBody}>
          <Text>No symptoms today</Text>
        </Body>
      </CardItem>
    </Card>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  cardBody: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 50,
  },
  cardHeader: {
    justifyContent: 'center',
  },
  greet: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  image: {
    width: '25%',
    aspectRatio: 1,
  },
  greetText: {
    fontSize: 18,
    marginRight: 30,
  },
});

export default Dashboard;
