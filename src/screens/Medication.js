import React, {useState} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {Card, CardItem, Text} from 'native-base';
import {modArray} from '../Utils';
import ListItem from '../components/ListItem';
import {Actions} from 'react-native-router-flux';

const medications = [
  {id: 0, name: 'Analgin', time: new Date('2019-12-11T03:24:00')},
  {id: 1, name: 'Aspirin', time: new Date('2019-12-17T12:24:00')},
  {id: 2, name: 'Ibuprofen', time: new Date('2019-12-01T19:24:00')},
];

const Medication = () => {
  const [checkedMeds, setChecked] = useState([]);
  const renderSeparator = () => <View style={styles.separator} />;

  const checkMed = id => {
    const checked = modArray(checkedMeds, id);
    setChecked(checked);
  };

  return (
    <View style={styles.container}>
      <Card>
        <CardItem header bordered style={styles.cardHeader}>
          <Text>Medication</Text>
        </CardItem>
        <CardItem bordered style={styles.cardBody}>
          <FlatList
            data={medications}
            keyExtractor={item => item.id.toString()}
            ItemSeparatorComponent={renderSeparator}
            renderItem={({item}) => (
              <ListItem
                {...item}
                isChecked={checkedMeds.includes(item.id)}
                check={checkMed}
                onPress={() => Actions.medsDetails({item})}
              />
            )}
          />
        </CardItem>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  cardBody: {
    width: '100%',
    paddingVertical: 10,
  },
  cardHeader: {
    justifyContent: 'center',
  },
  greet: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  image: {
    width: '25%',
    aspectRatio: 1,
  },
  greetText: {
    fontSize: 18,
    marginRight: 30,
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#D3D3D3',
  },
});

export default Medication;
