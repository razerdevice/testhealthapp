import React from 'react';
import {View, StyleSheet, Text, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

const Calendar = () => (
  <View style={styles.container}>
    <View style={styles.textWrapper}>
      <Text style={styles.title}>Calendar, Nothing here</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    alignItems: 'center',
  },
  textWrapper: {
    marginTop: height * 0.2,
    width: width * 0.4,
  },
  title: {
    fontSize: 28,
    textAlign: 'center',
  },
});

export default Calendar;
