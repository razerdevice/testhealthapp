module.exports = {
  root: true,
  extends: [
    '@react-native-community',
    'prettier',
    'prettier/flowtype',
    'prettier/react',
    'prettier/standard',
  ],
  plugins: ['prettier'],
};
